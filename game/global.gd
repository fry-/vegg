extends Node

var current_scene = null
var game_on = false
var players_in = [false,false,false,false]
var players_gamepad = [false,false,false,false]

var gametype = 0
var max_score = 5
var rounds = 3
var round_number = 1
var player_score = [null,null,null,null]
var player_round_score = [null,null,null,null]
var winning_players = []
var highest_score = 0

var game_area = Rect2(0,0,1024,600)

var player_color = [ \
	Color(0.91, 0.47, 0.94), \
	Color(0.33, 0.75, 0.72), \
	Color(0.95, 0.72, 0.04), \
	Color(0.99, 0.28, 0.19)]


func _ready():

	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)

	set_process_unhandled_input(true)
	var root = get_tree().get_root()
	current_scene = root.get_child( root.get_child_count() -1 )

func current_scene():
	var root = get_tree().get_root()
	current_scene = root.get_child( root.get_child_count() -1 )
	return current_scene

func _unhandled_input(event):
	if event.is_action_pressed("quit"):
		get_tree().quit() # default behavior
	if event.is_action_pressed("ui_fullscreen"):
		OS.set_window_fullscreen(!OS.is_window_fullscreen())

func quit():
	get_tree().quit()

func goto_scene(path):
	call_deferred("_deferred_goto_scene",path)

func _deferred_goto_scene(path):
	current_scene.free()
	var s = ResourceLoader.load(path)
	current_scene = s.instance()

	get_tree().get_root().add_child(current_scene)
	get_tree().set_current_scene(current_scene)

func update_winning():
	var found = false
	var w_players = []
	for n in range(max_score):
		if !found:
			for i in range(player_score.size()):
				if (player_score[i] == max_score-n):
					w_players.append(i)
					found = true
	highest_score = player_score[w_players[0]]
	winning_players = w_players

