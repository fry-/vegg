extends Node2D

var point = preload("res://objects/roundscreen/point.tscn")
onready var color = get_node("/root/global").player_color

onready var max_score = get_node("/root/global").max_score
onready var player_score = get_node("/root/global").player_score
onready var player_round_score = get_node("/root/global").player_round_score

var player_number = 2

func _ready():
	for n in range(max_score):
		var point_instance = point.instance()
		add_child(point_instance)
		point_instance.set_pos(Vector2(40+(n*20),0))
		if player_score[player_number] != null:
			if n <= player_score[player_number]-1:
				point_instance.change_color(color[player_number])
			if (n >= player_score[player_number] && n < player_score[player_number] + player_round_score[player_number]) :
				point_instance.change_color(Color(1,1,1))

func setup(plnumber):
	player_number = plnumber
