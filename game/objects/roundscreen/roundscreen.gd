extends Control

var next = false
var winning_players

onready var root = get_node("/root/global")

func _ready():
	set_process_input(true)

func _on_Timer_timeout():
	get_node("Label").set_hidden(false)
	next = true

func _input(event):
	if (event.type == InputEvent.JOYSTICK_BUTTON or event.type == InputEvent.KEY):
		if(event.is_pressed()):
			if(next):
				for n in range(root.player_score.size()):
					if root.player_score[n] != null:
						root.player_score[n] = root.player_score[n] + root.player_round_score[n]

				root.update_winning()

				if root.highest_score >= root.max_score:
					# Reset everything
					root.player_score = [null,null,null,null]
					root.round_number = 1
					root.players_in = [false,false,false,false]
					root.players_gamepad = [false,false,false,false]
					root.highest_score = 0

					root.goto_scene("res://objects/menu/menu.tscn")
				else:
					root.goto_scene("res://objects/game/game.tscn")