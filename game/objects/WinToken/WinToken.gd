extends Node2D

# Player Parameters
var player_number = 0
var color = Color(1, 1, 1)
var player_score = 0
var target

func setup(player, player_color, target_object):
	target = target_object
	player_number = player
	color = player_color

func _draw():
	draw_token( color )

func draw_token( color ):
	draw_circle(Vector2(0,0), 3, color)