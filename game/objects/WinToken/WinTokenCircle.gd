extends Node2D

# Player Parameters
var player_number = 0
var color = Color(1, 1, 1)
var target

var first = true

func _ready():
	pass

func setup(player, player_color, player_target, offset):
	player_number = player
	color = player_color
	target = player_target
	set_pos(Vector2(0, -offset*10))
	set_process(true)

func _draw():
	draw_blast( color )

func draw_blast( color ):
	draw_circle(Vector2(0,0), 3, color)

func _process(delta):
	pass