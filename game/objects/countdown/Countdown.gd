extends Node2D

# Nodes
var timer
var label

onready var sound = get_node("Sound")

# Timer Parameters
var time_left
var timer_active = true

func _ready():
	set_process(true)
	timer = get_node("Timer")
	label = get_node("Label")
	timer.connect("timeout", self, "_on_Timer_timeout")

func restart_timer():
	timer.start()
	timer_active = true

func _on_Timer_timeout():
	var game = get_tree().get_root().get_node("game")
	game.start_game()
	queue_free()

func _process(delta):
	if(timer_active):
		var new_time = int(timer.get_time_left())
		if(new_time != time_left):
			sound.play("countdown")
			time_left = new_time

			label.set_text(str(time_left+1))