 extends KinematicBody2D

# Preloading scenes
var wall = preload("res://objects/wall_generator/wall_generator.tscn")
var sword = preload("res://objects/sword/sword.tscn")
var blast = preload("res://objects/blast/blast.tscn")
var win_token = preload("res://objects/WinToken/WinToken.tscn")

onready var sound = get_node("Sound")

# Player Parameters
var player_number = 0
var player_velocity = Vector2()
var player_speed = 300
onready var color = get_node("/root/global").player_color

# Controls
var gamepad = true
var deadzone = 0.20
var outer_deadzone = 0.80
var deadzone_scale = 1/(outer_deadzone-deadzone)

# Weapons
var shooting = false
var slashing = false
var wall_reloading = false
var wall_reload_time = 0.01
var energy = 0
var sword_instance
var sword_energy_cost = 99

# Timers
var wall_timer
var sword_timer
var death_timer
var death_time = 0.05

var winning = true

onready var current_scene = get_node("/root/global").current_scene()

func _ready():
	set_process(true)
	set_process_unhandled_input(true)
	set_fixed_process(true)

	wall_timer = get_node("WallTimer")
	wall_timer.connect("timeout", self, "_on_Wall_Timer_timeout")
	wall_timer.set_wait_time( wall_reload_time )

	death_timer = get_node("DeathTimer")
	death_timer.connect("timeout", self, "_on_Death_Timer_timeout")
	death_timer.set_wait_time( death_time )

	sword_timer = get_node("SwordTimer")
	sword_timer.connect("timeout", self, "_on_Sword_Timer_timeout")

	sound.play("join")


func _process(delta):
	if(shooting):
		shoot()


func _fixed_process(delta):
	if((!shooting and !slashing) and energy < 100):
		energy = (energy + 1) * 1.03

	if Input.is_action_pressed(str("player",player_number,"_left")):
		player_velocity.x = -1
	elif Input.is_action_pressed(str("player",player_number,"_right")):
		player_velocity.x = 1

	if Input.is_action_pressed(str("player",player_number,"_up")):
		player_velocity.y = -1
	elif Input.is_action_pressed(str("player",player_number,"_down")):
		player_velocity.y = 1

	if(!gamepad):
		if !slashing:
			var mpos = get_global_mouse_pos()
			look_at(mpos)

	var motion = (player_velocity.clamped(1) * player_speed * delta)
	# Apply motion
	if (is_colliding()):
		var n = get_collision_normal()
		motion = n.slide(motion)
		player_velocity = n.slide(player_velocity)

		var collider = get_collider()
		if(collider != null):
			if(collider.is_in_group("wall") and !collider.is_in_group(str("player",player_number))):
				if collider.hot == true:
					collider.die(0)
					die()
	if slashing:
		var angle = get_rot()
		var direction = Vector2(sin(angle), cos(angle))
		motion = (direction.normalized() * player_speed * delta * 4)
	move(motion)

func _unhandled_input(event):
	var direction
	# Aiming
	if !slashing:
		direction = Vector2(Input.get_joy_axis(player_number,2), Input.get_joy_axis(player_number,3))
		if ((direction.x <= deadzone && direction.y <= deadzone) && (direction.x >= -deadzone && direction.y >= -deadzone)):
			pass
		else:
			rotate_player(direction)

	# Weapons
	if (energy >= 100 and event.is_action_pressed(str("player",player_number,"_fire"))):
		shooting = true
	if event.is_action_released(str("player",player_number,"_fire")):
		shooting = false
	if event.is_action_pressed(str("player",player_number,"_slash")):
		slash()

	# Movement
	player_velocity.x = Input.get_joy_axis(player_number,0)
	player_velocity.y = Input.get_joy_axis(player_number,1)
	player_velocity = deadzone(player_velocity)


func setup(player, using_gamepad):
	player_number = player
	gamepad = using_gamepad
	blast()

	if get_node("/root/global").round_number > 1:
		if get_node("/root/global").player_score[player_number] == get_node("/root/global").highest_score:
			var win_token_instance = win_token.instance()

			self.add_child(win_token_instance)
			win_token_instance.add_to_group(str("player",player_number))
			win_token_instance.set_pos(Vector2(0,-30))
			win_token_instance.setup(player_number, color[player_number], self)

	get_node("Graphics").set_color(color[player])

func die():
	if 	get_node("/root/global").game_on:
		if !current_scene.is_player_dead(player_number):
			blast()
			current_scene.kill_player(player_number)
			death_timer.start()

func rotate_player(direction):
	set_rot(direction.angle())

func deadzone(joy_axis):
	if (joy_axis.length() < deadzone || joy_axis.length() > outer_deadzone):

		# Check X Deadzones
		if (joy_axis.x < deadzone && joy_axis.x > -deadzone):
			joy_axis.x = 0
		elif (joy_axis.x > outer_deadzone || joy_axis.x < -outer_deadzone):
			if (joy_axis.x > 0):
				joy_axis.x = outer_deadzone
			else:
				joy_axis.x = -outer_deadzone

		# Check Y Deadzones
		if (joy_axis.y < deadzone && joy_axis.y > -deadzone):
			joy_axis.y = 0
		elif (joy_axis.y > outer_deadzone || joy_axis.y < -outer_deadzone):
			if (joy_axis.y > 0):
				joy_axis.y = outer_deadzone
			else:
				joy_axis.y = -outer_deadzone

	# Scale X to 0 and 1
	if (joy_axis.x > deadzone):
		joy_axis.x = ((joy_axis.x - deadzone) * deadzone_scale )
	elif (joy_axis.x < -deadzone):
		joy_axis.x = ((joy_axis.x + deadzone) * deadzone_scale )

	# Scale Y to 0 and 1
	if (joy_axis.y > deadzone):
		joy_axis.y = ((joy_axis.y - deadzone) * deadzone_scale )
	elif (joy_axis.y < -deadzone):
		joy_axis.y = ((joy_axis.y + deadzone) * deadzone_scale )

	return joy_axis

func _on_Wall_Timer_timeout():
	wall_reloading = false

func _on_Death_Timer_timeout():
	queue_free()

func _on_Sword_Timer_timeout():
	set_collision_mask_bit(0, true)
	set_layer_mask_bit(0, true)
	slashing = false
	sword_instance.destroy()

func shoot():
	if(!wall_reloading and energy >= 100):
		var player_position = get_global_pos()
		var player_rotation = get_rot()
		var wall_instance = wall.instance()

		energy = 0
		current_scene.add_child(wall_instance)
		wall_instance.add_to_group(str("player",player_number))
		wall_instance.set_rot(player_rotation)
		wall_instance.set_pos(player_position+Vector2(0,0))
		wall_instance.setup(player_number, get_rot(), color[player_number])

		wall_reloading = true
		wall_timer.start()

func slash():
	if(energy > sword_energy_cost):
		set_collision_mask_bit(0, false)
		set_layer_mask_bit(0, false)
		slashing = true
		energy = 0
		sword_instance = sword.instance()
		add_child(sword_instance)
		sword_instance.add_to_group(str("player",player_number))
		sword_instance.setup(player_number, color[player_number])
		sword_timer.start()
		get_node("SwordParticles").set_emitting(true)

func unslash():
	if(slashing):
		slashing = false
		sword_instance.destroy()

func get_player_number():
	return player_number

func get_player_color():
	return color[player_number]

func blast():
	var blast_instance = blast.instance()
	blast_instance.setup(player_number, color[player_number])
	blast_instance.add_to_group(str("player",player_number))
	add_child(blast_instance)

func get_energy():
	return(energy)