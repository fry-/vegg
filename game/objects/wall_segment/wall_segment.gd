extends KinematicBody2D

# Wall Parameters
var hot = true

# Player Parameters
var color = Color(0.0, 0.0, 1.0)
var player = 0

var blast = preload("res://objects/blast/blast.tscn")
onready var current_scene = get_node("/root/global").current_scene()

onready var wall_generator = get_node("../../")

func _ready():
	set_fixed_process(true)

func setup(player_number, player_color):
	player = player_number
	color = player_color
	get_node("Graphics").set_color(color)	

func blast():
	var blast_instance = blast.instance()
	blast_instance.setup(player, color)
	blast_instance.add_to_group(str("player",player))
	blast_instance.set_pos(get_global_pos())
	current_scene.add_child(blast_instance)
	pass

func die(blast):
	if blast:
		blast()
	wall_generator.stop()
	self.queue_free()

func _on_hot_timer_timeout():
	hot = false
