extends ButtonGroup

#onready var btns = get_children()
#var focus = false

func _ready():
	get_node("Play").grab_focus()
#	set_process_input(true)
#	set_process(true)

#func _process(delta):
#	pass

#func _input(event):
#	# Use Analog stick on menu
#	if (event.type == InputEvent.JOYSTICK_MOTION && !event.is_echo()):
#		if(event.is_action("ui_stick_up") or event.is_action("ui_stick_down")):
#			if(event.value > -0.1 && event.value < 0.1):
#				focus = true;
#			else:
#				if(event.is_action("ui_stick_up") && focus):
#					print("up")
#					focus = false;
#				elif(event.is_action("ui_stick_down") && focus):
#					print("down")
#					focus = false


func _on_Play_pressed():
	global.goto_scene("res://objects/PlayerOptions/PlayerOptions.tscn")

func _on_Settings_pressed():
	print("Settings")

func _on_Quit_pressed():
	global.quit()