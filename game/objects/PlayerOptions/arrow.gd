extends Node2D

var color = Color(0.3, 0.3, 0.3)
var size = 20

func _ready():
	pass

func _draw():
	draw_triangle( color )

func draw_triangle( color ):
	var points = Vector2Array()
	var colors = ColorArray([color])

	points.push_back(Vector2(-(size/2),-size/2))
	points.push_back(Vector2((size/2),-size/2))
	points.push_back(Vector2(0,size/2))

	draw_polygon(points, colors)