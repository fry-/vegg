extends Node2D

onready var sound = get_node("Sound")

# Scene Preload
var wall_segment = preload("res://objects/wall_segment/wall_segment.tscn")

# Wall Parameters
var wall_gap = 16
var wall_range = 100

# Utilities
var counter = 0
var timer

# Player Parameters
var player_number = 0
var color
var rotation = Vector2(0,0)

var generate_walls = true


onready var spawnpoint = get_node("spawnpoint")

func _ready():

	sound.play("wall")
	set_fixed_process(true)

func _fixed_process(delta):
	if (counter < wall_range) && generate_walls:
		for i in range(2):
			if generate_walls:
				var wall_segment_instance = wall_segment.instance()
				wall_segment_instance.setup(player_number, color)
				wall_segment_instance.add_to_group(str("player",player_number))
				var position = spawnpoint.get_pos()+Vector2(0,(wall_gap)*counter)
				wall_segment_instance.set_pos(position)
				spawnpoint.add_child(wall_segment_instance)
				counter = counter + 1
				if !get_node("/root/global").game_area.has_point(wall_segment_instance.get_global_pos()):
					stop()

func setup(number, player_rotation, player_color):
	player_number = number
	rotation = player_rotation
	color = player_color

func stop():
	generate_walls = false