extends Area2D

onready var sound = get_node("Sound")
onready var current_scene = global.current_scene()

var timer

# Player Parameters
var player_number = 0
var color

# Blast Parameters
var shake_power = 10

func _ready():
	set_timer()
	connect("body_enter", self, "_on_body_enter")

func _on_body_enter(body):
	if (body.is_in_group("wall")):
		body.die(0)

func _draw():
	draw_blast( color )

func setup(player, player_color):
	player_number = player
	color = player_color

func set_timer():
	timer = get_node("Timer")
	timer.connect("timeout", self, "_on_Timer_timeout")
	timer.set_wait_time(0.05)
	timer.start()

func _on_Timer_timeout():
	queue_free()

func draw_blast( color ):
	var camera = current_scene.get_node("Camera")
	if camera != null:
		sound.play("blast")
		camera.shake(shake_power)
		var colors = ColorArray([color])
		draw_circle(Vector2(0,0), 50, color)